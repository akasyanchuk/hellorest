package com.testrest.HelloREST;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.testrest.HelloREST.data.Product;


@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloRestApplicationTests {
	
  @Test
  public void productShouldHaveProperId() {
	  var product = new Product(23, "Samsung");
	  org.junit.Assert.assertTrue(product.getId() == 23);
  }
}