package com.testrest.HelloREST.infrastructure.repositories;

import org.springframework.data.repository.CrudRepository;

import com.testrest.HelloREST.data.db.*;

public interface UserRepository extends CrudRepository<User, Integer> {

}
