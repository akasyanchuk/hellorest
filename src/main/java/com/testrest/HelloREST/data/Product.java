package com.testrest.HelloREST.data;

import java.io.Serializable;
import java.util.Comparator;
import static java.util.Comparator.comparing;

public class Product implements Serializable //, Comparable<Product>
{
	public static final Comparator<Product> COMPARABLE_PRODUCT_BY_ID = comparing(Product::getId);
	private static final long serialVersionUID = -168823803057288185L;
	private Integer Id;
	private String Name;
	private Float Price;

	public Product() {}
	
	public Product(Integer id, String name) 
	{
		Id = id;
		Name = name;
	}
	
	public Integer getId() 
	{
		return Id;
	}
	
	public void setId(Integer id) 
	{
		Id = id;
	}
	
	public String getName() 
	{
		return Name;
	}
	
	public void setName(String name) 
	{
		Name = name;
	}
	
	public float getPrice() {
		return Price;
	}

	public void setPrice(Float price) {
		Price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		result = prime * result + ((Name == null) ? 0 : Name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		return true;
	}

	/*
	@Override
	public int compareTo(Product o) {
		if (getId() < o.getId())
		{
			return -1;
		}
		
		if (getId() > o.getId())
		{
			return 1;
		}
		
		return 0;
	}
	*/	
}
