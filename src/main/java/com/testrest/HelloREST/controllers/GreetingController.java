package com.testrest.HelloREST.controllers;

import com.testrest.HelloREST.data.*;
import com.testrest.HelloREST.data.db.*;
import com.testrest.HelloREST.infrastructure.repositories.*;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/greeting")
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    
    @Autowired
    private UserRepository userRepository;

    @GetMapping("say/to/{name}")
    public Greeting greeting(@PathVariable(required = true)
    						 String name) 
    {
        var result = generateGreeting(name);
        return result;
    }
    
    @GetMapping("add/{name}/{email}")
    public Greeting addUser(@PathVariable(required = true)String name, 
    					  @PathVariable(required = true)String email) 
    {    	
    	var user = new User();
    	user.setName(name);
    	user.setEmail(email);
    	
    	Greeting result;
    	try 
    	{	 
	    	userRepository.save(user);
	    	result = generateGreeting(name);
    	}
    	catch (Exception ex) 
    	{
    		System.err.println(ex);
    		result = generateGreeting(name + ". This user has not been created due to an error! Try again later.");
    	}
    	    	   	
    	return result;
    }
    
    @GetMapping("getAll")
    public Iterable<User> getAllUsers()
    { 
    	var users = userRepository.findAll();
    	
    	return users;
    }
    
    private Greeting generateGreeting(String name) {
    	var id = counter.incrementAndGet();
    	var message = String.format(template, name);
        var result = new Greeting(id, message);
        
        return result;
    }
}
