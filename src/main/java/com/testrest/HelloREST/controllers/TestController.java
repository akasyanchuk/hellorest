package com.testrest.HelloREST.controllers;

import java.util.*;

import org.springframework.web.bind.annotation.*;

import com.testrest.HelloREST.data.Product;

import static com.testrest.HelloREST.data.Product.COMPARABLE_PRODUCT_BY_ID;

@RestController
@RequestMapping(path = "/test")
public class TestController 
{	
	@RequestMapping(method = RequestMethod.GET, path = "getproducts")
	public Collection<Product> getProducts() 
	{
		var products = new ArrayList<Product>();
		products.add(new Product(1, "LG Flatroon"));
		products.add(new Product(2, "Samsung"));		
		return  products;
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "sets/hash")
	public String testHashSet() 
	{
		Set<Product> products = new HashSet<>();
		
		var p1 = new Product(1, "LG Flatroon");
		p1.setPrice(256.9f);
		var p2 = new Product(2, "Samsung");
		p2.setPrice(200f);
		var p3 = new Product(1, "LG Flatroon");
		p3.setPrice(220f);
		
		products.add(p1);
		products.add(p2);
		products.add(p3);
		
		return "The size of set is " + products.size();
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "sets/tree")
	public String testTreeSet() 
	{
		SortedSet<Product> products = new TreeSet<>(COMPARABLE_PRODUCT_BY_ID);
		products.add(new Product(1, "LG Flatroon"));
		products.add(new Product(2, "Samsung"));
		products.add(new Product(1, "LG Flatroon"));
		
		return "The size of set is " + products.size();
	}
	
	@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, path = "fi")
	public String testFunctionalInterfaces()
	{
		List<Runnable> runQueue = new ArrayList<>();
		runQueue.add(this::testRun1);
		
		for(var item : runQueue)
		{
			item.run();
		}
		
		return "Executed OK";
	}
	
	private void testRun1() 
	{
		System.out.println("Output from testRun1");
	}
}
